package main

import (
	"github.com/zzice209/backend/config"
	"github.com/zzice209/backend/database"
	"github.com/zzice209/backend/models"
	"github.com/zzice209/backend/repositories"
)

func main() {
	database.ConnectToDB()
	db := database.GetDBInstance()
	db.AutoMigrate(&models.Contact{})

	contactRepository := repositories.NewContactRepository(db)
	route := config.SetupRoutes(contactRepository)
	route.Run(":1323")
}
