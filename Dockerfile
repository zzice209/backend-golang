FROM golang:1.14-alpine as builder
WORKDIR /app
COPY go.mod .
COPY go.sum .

RUN go mod download
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -o server
RUN ls -la

FROM alpine:latest
RUN apk --no-cache add ca-certificates
RUN apk add --no-cache tzdata
RUN mkdir /app
WORKDIR /app
COPY --from=builder /app/server .
RUN ls -la
CMD ./server