package services

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/zzice209/backend/dtos"
	"github.com/zzice209/backend/models"
	"github.com/zzice209/backend/repositories"
	"log"
)

func CreateContact(contact *models.Contact, repository repositories.ContactRepository) dtos.Response {
	uuidResult, err := uuid.NewRandom()

	if err != nil {
		log.Fatalln(err)
	}

	contact.ID = uuidResult.String()
	operationResult := repository.Save(contact)

	if operationResult.Error != nil {
		return dtos.Response{Success: false, Message: operationResult.Error.Error()}
	}

	var data = operationResult.Result.(*models.Contact)
	return dtos.Response{Success: true, Data: data}

}

func FindAllContacts(repository repositories.ContactRepository) dtos.Response {
	operationResult := repository.FindAll()

	if operationResult.Error != nil {
		return dtos.Response{Success: false, Message: operationResult.Error.Error()}
	}

	var datas = operationResult.Result.(*models.Contacts)
	return dtos.Response{Success: true, Data: datas}
}

func FindOneContactById(id string, repository repositories.ContactRepository) dtos.Response {
	operationResult := repository.FindOneById(id)

	if operationResult.Error != nil {
		return dtos.Response{Success: false, Message: operationResult.Error.Error()}
	}

	var data = operationResult.Result.(*models.Contact)
	return dtos.Response{Success: true, Data: data}
}

func UpdateContactById(id string, contact *models.Contact, repository repositories.ContactRepository) dtos.Response {
	existingContactResponse := FindOneContactById(id, repository)

	if !existingContactResponse.Success {
		return existingContactResponse
	}

	existingContact := existingContactResponse.Data.(*models.Contact)
	existingContact.Name = contact.Name
	existingContact.Email = contact.Email
	existingContact.Address = contact.Address

	operationResult := repository.Save(existingContact)

	if operationResult.Error != nil {
		return dtos.Response{Success: false, Message: operationResult.Error.Error()}
	}

	return dtos.Response{Success: true, Data: operationResult.Result}
}

func DeleteOneContactId(id string, repository repositories.ContactRepository) dtos.Response {
	operationResult := repository.DeleteOneId(id)

	if operationResult.Error != nil {
		return dtos.Response{Success: false, Message: operationResult.Error.Error()}
	}
	return dtos.Response{Success: true}
}

func DeleteContactByIds(multiId *dtos.MultiID, repository repositories.ContactRepository) dtos.Response {
	operationResult := repository.DeleteByIds(&multiId.Ids)

	if operationResult.Error != nil {
		return dtos.Response{Success: false, Message: operationResult.Error.Error()}
	}

	return dtos.Response{Success: true}
}

func Pagination(repository repositories.ContactRepository, context *gin.Context, pagination *dtos.Pagination) dtos.Response {
	operationResult, totalPages := repository.Pagination(pagination)

	if operationResult.Error != nil {
		return dtos.Response{Success: false, Message: operationResult.Error.Error()}
	}

	var data = operationResult.Result.(*dtos.Pagination)
	// get current url path
	urlPath := context.Request.URL.Path

	// set first & last page pagination response
	data.FirstPage = fmt.Sprintf("%s?limit=%d&page=%d&sort=%s", urlPath, pagination.Limit, 0, pagination.Sort)
	data.LastPage = fmt.Sprintf("%s?limit=%d&page=%d&sort=%s", urlPath, pagination.Limit, totalPages, pagination.Sort)

	if data.Page > 0 {
		// set previous page pagination response
		data.PreviousPage = fmt.Sprintf("%s?limit=%d&page=%d&sort=%s", urlPath, pagination.Limit, data.Page-1, pagination.Sort)
	}

	if data.Page < totalPages {
		// reset previous page
		data.PreviousPage = ""
	}
	return dtos.Response{Success: true, Data: data}
}
