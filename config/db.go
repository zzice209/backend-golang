package config

import (
	"fmt"
	"os"
)

const (
	DBType = "postgres"
)

func GetDBType() string {
	return DBType
}

func GetPostgresConnectionString() string {
	DBUser := os.Getenv("DB_USER")
	DBPassword := os.Getenv("DB_PASSWORD")
	DBName := os.Getenv("DB_NAME")
	DBHost := os.Getenv("DB_HOST")
	DBPort := os.Getenv("DB_PORT")

	database := fmt.Sprintf( "host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		DBHost,
		DBPort,
		DBUser,
		DBPassword,
		DBName)
	return database
}
