package config

import (
	"github.com/gin-gonic/gin"
	"github.com/zzice209/backend/dtos"
	"github.com/zzice209/backend/helpers"
	"github.com/zzice209/backend/models"
	"github.com/zzice209/backend/repositories"
	"github.com/zzice209/backend/services"
	"net/http"
)

func SetupRoutes(contactRepository *repositories.ContactRepository) *gin.Engine {
	route := gin.Default()

	route.GET("/hello", func(c *gin.Context) {
		c.String(http.StatusOK, "Hello")
	})

	route.GET("/", func(c *gin.Context) {
		code := http.StatusOK

		response := services.FindAllContacts(*contactRepository)

		if !response.Success {
			code = http.StatusBadRequest
		}
		c.JSON(code, response)
	})

	route.GET("/show/:id", func(c *gin.Context) {
		id := c.Param("id")
		code := http.StatusOK
		response := services.FindOneContactById(id, *contactRepository)

		if !response.Success {
			code = http.StatusBadRequest
		}
		c.JSON(code, response)
	})

	route.PUT("/update/:id", func(c *gin.Context) {
		id := c.Param("id")
		var contact models.Contact

		err := c.ShouldBindJSON(&contact)

		// validation error
		if err != nil {
			response := helpers.GenerateValidationResponse(err)
			c.JSON(http.StatusBadRequest, response)
			return
		}

		code := http.StatusOK

		response := services.UpdateContactById(id, &contact, *contactRepository)

		if !response.Success {
			code = http.StatusBadRequest
		}
		c.JSON(code, response)
	})

	route.POST("/create", func(c *gin.Context) {
		var contact models.Contact

		// validate json
		err := c.ShouldBindJSON(&contact)

		// validation errors
		if err != nil {
			// generate validation errors response
			response := helpers.GenerateValidationResponse(err)
			c.JSON(http.StatusBadRequest, response)

			return
		}
		// default http status code = 200
		code := http.StatusOK

		// save contact & get it's response
		response := services.CreateContact(&contact, *contactRepository)

		// save contact failed
		if !response.Success {
			code = http.StatusBadRequest
		}
		c.JSON(code, response)
	})
	route.DELETE("delete/:id", func(c *gin.Context) {
		id := c.Param("id")

		code := http.StatusOK

		response := services.DeleteOneContactId(id, *contactRepository)

		if !response.Success {
			code = http.StatusBadRequest
		}
		c.JSON(code, response)
	})

	route.DELETE("delete", func(c *gin.Context) {
		var multiID dtos.MultiID

		err := c.ShouldBindJSON(&multiID)

		// validation errors
		if err != nil {
			response := helpers.GenerateValidationResponse(err)
			c.JSON(http.StatusBadRequest, response)
			return
		}
		if len(multiID.Ids) == 0 {
			response := dtos.Response{Success: false, Message: "IDs can not be blank"}
			c.JSON(http.StatusBadRequest, response)
			return
		}
		code := http.StatusOK
		response := services.DeleteContactByIds(&multiID, *contactRepository)

		if !response.Success {
			code = http.StatusBadRequest
		}
		c.JSON(code, response)
	})

	route.GET("/pagination", func(c *gin.Context) {
		code := http.StatusOK
		pagination := helpers.GeneratePaginationRequest(c)
		response := services.Pagination(*contactRepository, c, pagination)
		if !response.Success {
			code = http.StatusBadRequest
		}
		c.JSON(code, response)
	})
	return route
}
