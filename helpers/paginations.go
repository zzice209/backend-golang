package helpers

import (
	"github.com/gin-gonic/gin"
	"github.com/zzice209/backend/dtos"
	"strconv"
)

func GeneratePaginationRequest(c *gin.Context) *dtos.Pagination {
	// convert query parameter string to int
	limit, _ := strconv.Atoi(c.DefaultQuery("limit", "10"))
	page, _ := strconv.Atoi(c.DefaultQuery("page", "1"))
	sort := c.DefaultQuery("sort", "created_at desc")

	return &dtos.Pagination{Limit: limit, Page: page, Sort: sort}
}
