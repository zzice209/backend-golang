package helpers

import (
	"github.com/go-playground/validator/v10"
	"github.com/zzice209/backend/dtos"
	"github.com/zzice209/backend/langs"
)

func GenerateValidationResponse(err error) (response dtos.ValidationResponse) {
	response.Success = false

	var validations []dtos.Validation

	//get validation errors
	validationErrors := err.(validator.ValidationErrors)

	for _, value := range validationErrors {
		// get field & rule (tag)
		field, rule := value.Field(), value.Tag()
		validation := dtos.Validation{Field: field, Message: langs.GenerateValidationMessage(field, rule)}
		validations = append(validations, validation)
	}
	// Set Validations response
	response.Validations = validations
	return response
}
